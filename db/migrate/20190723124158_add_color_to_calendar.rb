class AddColorToCalendar < ActiveRecord::Migration[5.2]
  def change
    add_column :calendars, :color, :string, default: "#000000"
  end
end