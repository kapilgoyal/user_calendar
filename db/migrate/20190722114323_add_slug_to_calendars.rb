class AddSlugToCalendars < ActiveRecord::Migration[5.2]
  def change
    add_column :calendars, :slug, :string
    add_index :calendars, :slug, unique: true
  end
end
