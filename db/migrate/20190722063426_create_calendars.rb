class CreateCalendars < ActiveRecord::Migration[5.2]
  def change
    create_table :calendars do |t|
      t.string :title
      t.text :discription
      t.integer :user_id

      t.timestamps
    end
  end
end
