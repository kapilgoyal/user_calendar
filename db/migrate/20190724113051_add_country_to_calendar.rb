class AddCountryToCalendar < ActiveRecord::Migration[5.2]
  def change
    add_column :calendars, :country_code, :string
  end
end