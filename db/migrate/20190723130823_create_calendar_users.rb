class CreateCalendarUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :calendar_users do |t|
      t.integer :calendar_id
      t.integer :user_id

      t.timestamps
    end
  end
end