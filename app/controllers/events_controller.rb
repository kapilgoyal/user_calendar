class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]

  def index
    if params[:calendar_id].present?
      @calendar = Calendar.friendly.find(params[:calendar_id])
      @events = @calendar.events.where(start: params[:start]..params[:end])
    else
      @events = Event.select('DISTINCT ON (title,start) *').where(calendar_id: current_user.all_calendars.map(&:id), start: params[:start]..params[:end])
    end
  end

  def show
  end

  def new
    start_time = Time.at(params[:start_date].to_i/1000)
    end_time = Time.at((params[:end_date].to_i - 1) /1000)
    @event = Event.new(start: start_time, end: end_time, calendar_id: params[:main_calendar_id])
  end

  def edit
  end

  def create
    params[:event][:start] = event_params["start"].to_date.beginning_of_day
    params[:event][:end] = event_params["end"].to_date.end_of_day
    @event = Event.new(event_params)
    @event.user_id = current_user.id
    @event.save
    CalendarMailer.send_event_mail(@event).deliver_later
  end

  def update
    params[:event][:start] = event_params["start"].to_date.beginning_of_day
    params[:event][:end]= event_params["end"].to_date.end_of_day
    @event.update(event_params)
  end

  def destroy
    @event.destroy
  end

  private
    def set_event
      @event = Event.find(params[:id])
    end

    def event_params
      params.require(:event).permit(:title, :description, :start, :end, :calendar_id)
    end
end
