class AdminController < ApplicationController
	
	layout 'admin'

	skip_before_action :authenticate_user!

end