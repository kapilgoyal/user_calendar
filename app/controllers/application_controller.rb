class ApplicationController < ActionController::Base
	before_action :authenticate_user!
 	skip_before_action :verify_authenticity_token

	layout :layout_by_resource

	def after_sign_in_path_for(resource)
		if resource.class == AdminUser
			admin_dashboard_path
		else
		 welcome_index_path
		end
	end

	def after_sign_out_path_for(resource)
		if resource == :admin_user
			new_admin_user_session_path
		else
			new_user_session_path
		end
	end

	private

  def layout_by_resource
    if devise_controller? && resource_name == :admin_user
      "admin"
    else
      "application"
    end
  end

end
