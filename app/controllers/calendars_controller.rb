class CalendarsController < ApplicationController
  before_action :set_calendar, only: [:show, :edit, :update, :destroy]

  # GET /calendars
  # GET /calendars.json
  def index
    @calendars = current_user.all_calendars
  end

  def view_all
    @calendars = current_user.all_calendars
  end

  def view_all_in_one
    # @calendars = current_user.all_calendars

    month = Date.today.month > 6 ? 6 : 0
    @start_date = params[:start_date].present? ? params[:start_date].to_date : Date.today.beginning_of_year + month.months
    @prev_date = @start_date - 6.months
    @next_date = @start_date + 6.months
  end

  # GET /calendars/1
  # GET /calendars/1.json
  def show
    month = Date.today.month > 6 ? 6 : 0
    @start_date = params[:start_date].present? ? params[:start_date].to_date : Date.today.beginning_of_year + month.months
    @prev_date = @start_date - 6.months
    @next_date = @start_date + 6.months
  end

  # GET /calendars/new
  def new
    @calendar = Calendar.new(user_id: current_user.id)
  end

  # GET /calendars/1/edit
  def edit
  end

  # POST /calendars
  # POST /calendars.json
  def create
    @calendar = Calendar.new(calendar_params)
    @calendar.user_id = current_user.id
    respond_to do |format|
      if @calendar.save

        emails = JSON.parse params[:calendar][:users] rescue []
        emails.each do |email|
          user = User.find_by_email(email)
          user = User.invite!(email: email) if user.nil?
          calendar_user = @calendar.calendar_users.create(user_id: user.id)
        end

        # User.where(id: params[:calendar][:users]).each do |user|
        #   calendar_user = @calendar.calendar_users.create(user_id: user.id)
        #   CalendarMailer.send_join_email(calendar_user).deliver_later
        # end

        format.html { redirect_to @calendar, notice: 'Calendar was successfully created.' }
        format.json { render :show, status: :created, location: @calendar }
      else
        format.html { render :new }
        format.json { render json: @calendar.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /calendars/1
  # PATCH/PUT /calendars/1.json
  def update
    respond_to do |format|
      if @calendar.update(calendar_params)
        if @calendar.user == current_user

          emails = JSON.parse params[:calendar][:users] rescue []
          user_ids = User.where(email: emails)
          @calendar.calendar_users.where.not(user_id: user_ids).destroy_all
          emails.each do |email|
            user = User.find_by_email(email)
            user = User.invite!(email: email) if user.nil?
            calendar_user = @calendar.calendar_users.find_or_create_by(user_id: user.id)
          end

          # @calendar.calendar_users.where.not(user_id: user_ids).destroy_all
          # User.where(id: user_ids).each do |user|
          #   if @calendar.calendar_users.where(user_id: user.id).first.nil?
          #     calendar_user = @calendar.calendar_users.create(user_id: user.id)
          #     CalendarMailer.send_join_email(calendar_user).deliver_later
          #   end
          # end

        end
        format.html { redirect_to @calendar, notice: 'Calendar was successfully updated.' }
        format.json { render :show, status: :ok, location: @calendar }
      else
        format.html { render :edit }
        format.json { render json: @calendar.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /calendars/1
  # DELETE /calendars/1.json
  def destroy
    @calendar.destroy
    respond_to do |format|
      format.html { redirect_to calendars_url, notice: 'Calendar was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_calendar
      @calendar = Calendar.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def calendar_params
      params.require(:calendar).permit(:title, :country_code, :color, :discription)
    end
end
