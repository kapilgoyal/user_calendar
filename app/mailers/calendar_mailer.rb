class CalendarMailer < ApplicationMailer

	def send_join_email(calendar_user)
		@calendar = calendar_user.calendar
		@user = calendar_user.user
    
    mail(
      subject: "You joined the calendar #{@calendar.title}",
      to: @user.email
    )
  end

  def send_event_mail(event)
  	@event = event
  	@calendar = @event.calendar
  	mail(to: @calendar.all_users.map(&:email), subject: "Event created in calendar #{@calendar.title}")
  end

end