class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
 	devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable

 	has_many :calendars

 	has_many :calendar_users

 	def all_calendars
 		calendar_ids = (calendars.ids + calendar_users.map(&:calendar_id)).uniq
 		Calendar.where(id: calendar_ids)
 	end
 	
end