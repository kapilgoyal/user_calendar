class Calendar < ApplicationRecord
	extend FriendlyId
  friendly_id :title, use: :slugged

  belongs_to :user

  has_many :events
  has_many :calendar_users
  has_many :users, through: :calendar_users

  validates_presence_of :title

  after_create :update_events

  def all_users
    user_ids = ([user.id] + users.ids).uniq
    User.where(id: user_ids)
  end

  def country_name
    country = ISO3166::Country[country_code]
    country.translations[I18n.locale.to_s] || country.name if country
  end

  def update_events
    # capi = Calendarific::V2.new(ENV['CALENDARIFIC_API_KEY'])
    # parameters = {'country' => country_code, 'year' => Date.today.year}
    # data = capi.holidays(parameters)
    # holidays = data["response"]["holidays"] rescue nil

    if self.country_code.present?
      begin
        (0..12).each do |i|
          # uri = URI.parse("https://calendarific.com/api/v2/holidays?&api_key=#{ENV['CALENDARIFIC_API_KEY']}&country=#{self.country_code}&year=#{Date.today.year + i}")
          # http = Net::HTTP.new(uri.host, uri.port)
          # http.use_ssl = true
          # request = Net::HTTP::Get.new(uri.request_uri)
          # response = http.request(request)
          # data = ActiveSupport::JSON.decode(response.body)
          # holidays = data["response"]["holidays"] rescue nil

          # if holidays.present?
          #   holidays.each do |holiday|
          #     self.events.create(title: holiday["name"], description: holiday[:description], start: holiday["date"]["iso"].to_date.beginning_of_day, end: holiday["date"]["iso"].to_date.end_of_day)
          #   end
          # end

          uri = URI.parse("https://date.nager.at/api/v2/PublicHolidays/#{Date.today.year + i}/#{self.country_code}")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          request = Net::HTTP::Get.new(uri.request_uri)
          response = http.request(request)
          data = ActiveSupport::JSON.decode(response.body)
          # holidays = data["response"]["holidays"] rescue nil

          if data.present?
            data.each do |holiday|
              self.events.create(title: holiday["name"], description: holiday["localName"], start: holiday["date"].to_date.beginning_of_day, end: holiday["date"].to_date.end_of_day)
            end
          end

        end
      rescue
      end
    end
    
  end

end