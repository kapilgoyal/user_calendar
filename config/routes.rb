Rails.application.routes.draw do
  
  resources :events
  resources :calendars do
    collection do
      get :view_all
      get :view_all_in_one
    end
  end
  namespace :admin do
    resources :users
  end
  devise_for :users, path: 'users'
  # devise_for :admin_users, path: 'admins', controllers: { sessions: "admin_users/sessions", registrations: "admin_users/registrations" }
  devise_for :admin_users, skip: [:sessions]
  as :admin_user do
    get 'admin', to: 'admin_users/sessions#new', as: :new_admin_user_session
    post 'admin', to: 'admin_users/sessions#create', as: :admin_user_session
    delete 'sign_out', to: 'admin_users/sessions#destroy', as: :destroy_admin_user_session, via: Devise.mappings[:admin_user].sign_out_via
  end
  # devise_for :admins
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
	get 'welcome/index'
  root 'welcome#index'
  get 'admin/dashboard'
 
end
